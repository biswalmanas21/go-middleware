package forms

type UpdateExpectedValue struct {
	Provider    string `json:"Provider" binding:"required"`
	Component   string `json:"Component" binding:"required"`
	Rampcode    string `json:"Rampcode" binding:"required"`
	Locale      string `json:"Locale" binding:"required"`
	Environment string `json:"Environment" binding:"required"`
	Region      string `json:"Region" binding:"required"`
	Expected    int    `json:"Expected" validate:"gte=0,required"`
	BfrTime     int    `json:"BfrTime" validate:"min=1,max=24,required"`
}

type Crypto struct {
	HashValue string `json:"Password" binding:"required"`
}

type UpdateExisting struct {
	ID          string `json:"_id" binding:"required"`
	Provider    string `json:"Provider" binding:"required"`
	Component   string `json:"Component" binding:"required"`
	Rampcode    string `json:"Rampcode" binding:"required"`
	Locale      string `json:"Locale" binding:"required"`
	Environment string `json:"Environment" binding:"required"`
	Region      string `json:"Region" binding:"required"`
	Expected    int    `json:"Expected" validate:"gte=0,required"`
	BfrTime     int    `json:"BfrTime" validate:"min=1,max=24,required"`
}
