package gcp

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	//"log"
	"os"
	"strings"
	"sync"

	"github.com/gin-gonic/gin"
	"golang.org/x/net/context"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/compute/v1"
)

type GcpController struct{}

func handlepanic() {

	if a := recover(); a != nil {
		fmt.Println("RECOVER", a)
	}
}

// Intializing Global authentication

func (g *GcpController) GetData(c *gin.Context) {

	defer handlepanic()
	gcpBackendPath := os.Getenv("GCPLB")
	fmt.Printf("Reading the config file ")
	jsonFile, err := os.Open(gcpBackendPath + "gcpbackends.json")
	if err != nil {
		fmt.Println(err)

	}
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	var x []GcpModel
	json.Unmarshal(byteValue, &x)
	lengthlist := len(x)
	fmt.Printf("\n%v", lengthlist)
	var q []ServerMetadata
	var wg sync.WaitGroup
	//Intializing google backend authentication

	if err != nil {
		fmt.Printf("\nFailed at GetActive 2nd err\n")
		fmt.Println(err)
	}
	wg.Add(lengthlist)

	for i := 0; i < lengthlist; i++ {
		go func(i int) {
			defer wg.Done()
			y := GetActiveAsg(x[i].Environment, x[i].LocationVal, x[i].BackendName)
			//fmt.Printf("%v", y)
			envval := x[i].Environment
			var envName string
			if (envval == "prodeu") || (envval == "prodkr") || (envval == "prodsg") || (envval == "produs") {
				envName = "prod"
			} else if envval == "stg" {
				envName = "stgus"
			} else {
				envName = envval
			}
			if len(y) == 0 {
				y = []ServerMetadata{
					ServerMetadata{
						Provider:    "GCP",
						Component:   x[i].Component,
						Environment: envName,
						Locale:      x[i].Locale,
						ContentCP:   x[i].ContentCP,
						Region:      x[i].LocationVal,
						Healthy:     0,
						TargetSize:  0,
						Messages:    "No server",
					},
				}
			}
			q = append(q, y...)

		}(i)
	}
	wg.Wait()
	jsonData, err := json.Marshal(q)
	if err != nil {
		fmt.Println(err)
	}
	// var w http.ResponseWriter
	//fmt.Printf("%v", string(jsonData))

	//c.JSON(200, gin.H{"Message": "Data Found", "data": string(jsonData)})
	c.Data(200, gin.MIMEJSON, jsonData)

}

func GetActiveAsg(environment, regionname, backendname string) []ServerMetadata {
	handlepanic()
	EnvInfo := map[string]string{}

	ctx := context.Background()
	con, err := google.DefaultClient(ctx, compute.CloudPlatformScope)
	if err != nil {
		fmt.Printf("\nFailed at GetActive 1st err\n")
		fmt.Println(err)
	}
	computeService, err := compute.New(con)

	//setting cred complete
	project := EnvInfo[environment]
	region := regionname
	backendService := backendname
	//,,

	//fmt.Printf("\nProject: %v , Region: %v , Backend: %v\n", project, region, backendService)
	resp, err := computeService.RegionBackendServices.Get(project, region, backendService).Context(ctx).Do()

	var x []ServerMetadata
	if err != nil {
		fmt.Printf("\nFailed at GetActive 3nd err\n")
		//fmt.Printf("for %v region %v", backendService, region)
		fmt.Println(err)

	} else {
		for _, item := range resp.Backends {
			igmName := item.Group
			metadata, active := ChecknExtract(project, region, igmName, computeService, ctx)
			if active == true {
				x = append(x, metadata)
			}

		}
	}

	///fmt.Printf("\n%v", x)
	return x
}

func ChecknExtract(projectname, regionname, igmName string, computeService *compute.Service, ctx context.Context) (returndata ServerMetadata, active bool) {
	handlepanic()
	active = true

	project := projectname

	region := regionname

	//fmt.Printf("\nInstanceGroup Manager : %v", igmName)
	instanceGroupManager := strings.Split(igmName, "/")[len(strings.Split(igmName, "/"))-1]
	igmArray := strings.Split(igmName, "/")
	locationType := igmArray[7]
	locarionVal := igmArray[8]
	var resp *compute.InstanceGroupManager
	var err error

	switch locationType {
	case "zones":
		resp, err = computeService.InstanceGroupManagers.Get(project, locarionVal, instanceGroupManager).Context(ctx).Do()
		fmt.Printf("\n Zonal Group :%v", instanceGroupManager)
	case "regions":
		resp, err = computeService.RegionInstanceGroupManagers.Get(project, locarionVal, instanceGroupManager).Context(ctx).Do()
	}

	if err != nil {
		fmt.Printf("\nFailed at checknextract 3rd err\n")
		fmt.Println(err)
	}

	TargetSize := resp.TargetSize
	Healthy := resp.CurrentActions.None
	if TargetSize == 0 {
		returndata.Provider = "GCP"
		returndata.TargetSize = 0
		returndata.Healthy = 0
		active = false
		return
	} else {
		active = true
		itmname := strings.Split(resp.InstanceTemplate, "/")[len(strings.Split(resp.InstanceTemplate, "/"))-1]
		dataVal := InstanceTemplate(project, itmname, computeService, ctx)
		//getting data from Perf
		if dataVal.component == "Perf-worker" {
			dataVal2 := getPerfData(project, locationType, locarionVal, instanceGroupManager, computeService, ctx)
			returndata.DeDEModel = dataVal2.dedemodel
			returndata.DeDETool = dataVal2.dedetool

		}
		//ends here Perf
		returndata.Provider = "GCP"
		returndata.TargetSize = TargetSize
		returndata.Healthy = Healthy
		returndata.Component = dataVal.component
		returndata.Locale = dataVal.locale
		returndata.Region = region
		returndata.Environment = dataVal.environment
		returndata.ContentCP = dataVal.ContentCP
		returndata.ASREngine = dataVal.asrengine
		returndata.ASRItn = dataVal.asritn
		returndata.ASRModel = dataVal.asrmodel
		returndata.ASRServer = dataVal.asrserver
		returndata.TTSEngine = dataVal.ttsengine
		returndata.TTSModel = dataVal.ttsmodel
		returndata.TTSServer = dataVal.ttsserver

		return
	}
	return
}

type InstanceMetadata struct {
	templateid  string
	component   string
	ContentCP   string
	environment string
	locale      string
	atsmodel    string `json:"atsmodel, omitempty"`
}

// InstanceTemplate Returns value to the gcp_asg package
func InstanceTemplate(projectid, instancetemplateid string, computeService *compute.Service, ctx context.Context) InstanceMetadata {
	handlepanic()
	//fmt.Printf("Getting Data of the Instance template")

	project := projectid
	res, err := computeService.InstanceTemplates.Get(project, instancetemplateid).Context(ctx).Do()
	if err != nil {
		fmt.Printf("\nFailed at itm 3rd err\n")
		fmt.Println(err)
	}
	templateid := res.Name
	metadatalist := res.Properties.Metadata.Items
	var component, environment, ContentCP, locale, asrmodel, asrengine, asrserver, asritn, ttsmodel, ttsengine, ttsserver, dedetool, frfrtool, itittool, esestool, engbtool, dedemodel, frfrmodel, ititmodel, esesmodel, engbmodel string
	for _, element := range metadatalist {
		matchCase := element.Key
		Value := element.Value
		switch matchCase {
		case "ansible_role":
			component = *Value
		case "env":
			environment = *Value
		case "contentCp":
			ContentCP = *Value
		case "ats_model":
			model = *Value
		case "ats_val":
			normalizer = *Value
		case "ats_serve":
			server = *Value
		case "ats_eng":
			engine = *Value

		case "ats_tool_version":
			detool = *Value

		}
	}
	data := InstanceMetadata{
		templateid:  templateid,
		component:   component,
		environment: environment,
		ContentCP:   ContentCP,
		locale:      locale,
	}
	return data
}

type PerfMetadata struct {
}

func getPerfData(projectname, locationType, locarionVal, itmname string, computeService *compute.Service, ctx context.Context) PerfMetadata {

	handlepanic()
	var x PerfMetadata
	//fmt.Printf("Getting Data of the Instance template")
	var resp1 *compute.InstanceGroupManagersListManagedInstancesResponse
	var resp2 *compute.RegionInstanceGroupManagersListInstancesResponse
	var err error
	project := projectname
	instanceGroupManager := itmname
	switch locationType {
	case "zones":
		zone := locarionVal
		resp1, err = computeService.InstanceGroupManagers.ListManagedInstances(project, zone, instanceGroupManager).Context(ctx).Do()
		if err != nil {
			fmt.Println(err)
		}
		for _, item := range resp1.ManagedInstances {
			instancemeta := item.Instance
			instanemetaarray := strings.Split(instancemeta, "/")
			locationzone := instanemetaarray[8]
			instancename := instanemetaarray[10]
			if item.CurrentAction == "NONE" {
				x = getPerfMetadata(projectname, locationzone, instancename, computeService, ctx)
				break
			}
		}
	case "regions":
		region := locarionVal
		resp2, err = computeService.RegionInstanceGroupManagers.ListManagedInstances(project, region, instanceGroupManager).Context(ctx).Do()
		if err != nil {
			fmt.Println(err)
		}
		for _, item := range resp2.ManagedInstances {
			instancemeta := item.Instance
			instanemetaarray := strings.Split(instancemeta, "/")
			locationzone := instanemetaarray[8]
			instancename := instanemetaarray[10]
			if item.CurrentAction == "NONE" {
				x = getPerfMetadata(projectname, locationzone, instancename, computeService, ctx)
				break
			}
		}
	}

	return x
}

func getPerfMetadata(projectname, locationzone, instancename string, computeService *compute.Service, ctx context.Context) PerfMetadata {
	project := projectname
	zone := locationzone
	instance := instancename
	resp, err := computeService.Instances.Get(project, zone, instance).Context(ctx).Do()
	if err != nil {
		fmt.Println(err)
	}
	var q PerfMetadata
	var dedetool, dedemodel string
	for _, item := range resp.Metadata.Items {
		key := item.Key
		Value := item.Value
		switch key {
		case "ats_de_tool_version":
			detool = *Value

		}
	}
	q.dedemodel = dedemodel
	q.dedetool = dedetool

	return q

}
