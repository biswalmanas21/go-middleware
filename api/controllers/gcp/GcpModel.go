package gcp

type GcpModel struct {
	Component   string `json:"Component"`
	Environment string `json:"Environment"`
	BackendName string `json:"BackendName"`
	Locale      string `json:"Locale"`
	ContentCP   string `json:"ContentCP"`
	Loctype     string `json:"Loctype"`
	LocationVal string `json:"LocationVal"`
}

type Modeldata struct {
	Data []GcpModel
}

type ServerMetadata struct {
	Provider    string `json:"Provider, omitempty"`
	Component   string `json:"Component"`
	Environment string `json:"Environment"`
	Locale      string `json:"Locale, omitempty"`
	Rampcode    string `json:"Rampcode, omitempty"`
	Region      string `json:"Region"`
	TargetSize  int64  `json:"TargetSize"`
	Healthy     int64  `json:"Healthy"`
	ATSModel    string `json:"ATSModel, omitempty"`
	ATSEngine   string `json:"ATSEngine, omitempty"`

	DeDETool string `json:"DeDETool, omitempty"`

	Messages string `json:"Message, omitempty"`
}

type ListserverMetadata struct {
	Data []ServerMetadata
}
