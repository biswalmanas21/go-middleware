package expected

import (
	"marinaserver/api/forms"
	"marinaserver/api/models"

	"github.com/gin-gonic/gin"
)

var ExpectCrypto = new(models.CryptoModel)

type CryptoController struct{}

func (e *CryptoController) FindCryptoOne(c *gin.Context) {
	dataValue := forms.Crypto{
		HashValue: c.Params.ByName("password"),
	}

	result, err := ExpectCrypto.GetCryptoVal(dataValue)
	if err != nil {
		c.JSON(404, gin.H{"message": "Data does not exist",
			"error": result})
		c.Abort()
		return
	}

	c.JSON(200, gin.H{
		"message": "Found value for given params",
		"data":    result})
}
