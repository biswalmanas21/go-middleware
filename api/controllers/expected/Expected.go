package expected

import (
	"fmt"
	"marinaserver/api/models"

	"marinaserver/api/forms"

	//"encoding/json"

	"github.com/gin-gonic/gin"
)

var ExpectedModel = new(models.ExpectedProviderModel)

type ExpectedController struct{}

func (e *ExpectedController) UpdateExpected(c *gin.Context) {

	var datavalue forms.UpdateExpectedValue

	if err := c.BindJSON(&datavalue); err != nil {
		// specified response
		fmt.Printf("%v", c)
		fmt.Printf("%v", datavalue)
		c.JSON(406, gin.H{
			"message": "Provide relevant fields",
			"error":   err})
		// abort the request
		c.Abort()
		// return nothing
		return
	}

	if (datavalue.BfrTime < 1) || (datavalue.BfrTime > 24) {
		c.JSON(406, gin.H{
			"message": "Time Parameter exceeded value . min 1 , max 24",
		})
		// abort the request
		c.Abort()
		// return nothing
		return
	}
	err := ExpectedModel.UpdateExpected(datavalue)
	if err != nil {
		c.JSON(400, gin.H{"message": "Problem creating Updated Value"})
		c.Abort()
		return
	}

	c.JSON(201, gin.H{
		"message": "New ExpectedValue was generated",
		"data":    datavalue})
}

func (e *ExpectedController) GetExpected(c *gin.Context) {
	c.Header("Access-Control-Allow-Origin", "*")

	result, err := ExpectedModel.GetExpectedValues()

	if err != nil {
		c.JSON(400, gin.H{"message": "Problem  Getting the details"})
		c.Abort()
		return
	}
	c.JSON(200, gin.H{
		"message": "All Values below",
		"data":    result})
}

func (e *ExpectedController) FindOne(c *gin.Context) {
	var datavalue forms.UpdateExpectedValue

	if err := c.BindJSON(&datavalue); err != nil {
		// specified response
		fmt.Printf("%v", c)
		fmt.Printf("%v", datavalue)
		c.JSON(406, gin.H{
			"message": "Provide relevant fields",
			"error":   err})
		// abort the request
		c.Abort()
		// return nothing
		return
	}
	result, err := ExpectedModel.GetExpectedResult(datavalue)
	if (err != nil) || (result == nil) {
		c.JSON(404, gin.H{"message": "Data does not exist"})
		c.Abort()
		return
	}

	c.JSON(200, gin.H{
		"message": "Found value for given params",
		"data":    result[0]})
}

func (e *ExpectedController) FindnUpdate(c *gin.Context) {
	var datavalue forms.UpdateExisting
	if err := c.BindJSON(&datavalue); err != nil {
		// specified response
		fmt.Printf("%v", c)

		c.JSON(406, gin.H{
			"message": "Provide relevant fields",
			"error":   err})
		c.Abort()
		return
	}
	if (datavalue.BfrTime < 1) || (datavalue.BfrTime > 24) {
		c.JSON(406, gin.H{
			"message": "Time Parameter exceeded value . min 1 , max 24",
		})
		// abort the request
		c.Abort()
		// return nothing
		return
	}
	result, err := ExpectedModel.UpdateExisting(datavalue)
	if err != nil {
		//data, _ := json.MarshalIndent(datavalue, "", "  ")
		fmt.Printf("%+v", datavalue)
		fmt.Printf("%v", err)
		c.JSON(500, gin.H{"message": "Internal server error"})
		c.Abort()
		return
	}
	c.JSON(200, gin.H{
		"message": "Existing Updated",
		"data":    result})

}
