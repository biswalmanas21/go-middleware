package aws

//"fmt"
import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"sync"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/ecs"
	"github.com/aws/aws-sdk-go/service/elb"
	"github.com/aws/aws-sdk-go/service/elbv2"

	"github.com/gin-gonic/gin"
)

type AwsController struct{}

func handlepanic() {

	if a := recover(); a != nil {
		fmt.Println("RECOVER", a)
	}
}
func (g *AwsController) GetData(c *gin.Context) {
	defer handlepanic()
	awsBackendPath := os.Getenv("AWSLB")
	fmt.Printf("Reading the config file ")
	jsonFile, err := os.Open(awsBackendPath + "awslb.json")
	if err != nil {
		panic(err)

	}
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	var x []AWSConfig
	json.Unmarshal(byteValue, &x)
	lengthlist := len(x)
	fmt.Printf("\n%v", lengthlist)

	var resultData []ServerMetadata
	var m MetadataTag2
	//create Waitgroup
	var wg sync.WaitGroup

	if err != nil {
		fmt.Printf("\nFailed at GetActive 2nd err\n")
		panic(err)
	}
	wg.Add(lengthlist)
	for i := 0; i < lengthlist; i++ {
		go func(i int) {
			defer wg.Done()
			resourceType := x[i].ResourceType
			var q ServerMetadata
			switch resourceType {
			case "classic":

				m = GetLoadbalancerHealthcheck(x[i].LoadName, x[i].Environment, x[i].Region)
				q.Provider = "AWS"
				q.ATSEngine = m.ATSEngine
				q.ATSItn = m.ATSItn
				q.ATSModel = m.ATSModel
				q.ATSServer = m.ATSServer
				q.Component = x[i].Component
				envval := x[i].Environment
				if (envval == "prdeu") || (envval == "prdkr") || (envval == "prdsg") || (envval == "prdus") {
					q.Environment = "prod"
				} else if envval == "stg" {
					q.Environment = "stgus"
				} else {
					q.Environment = envval
				}

				q.Healthy = m.Healthy
				q.Locale = x[i].Locale
				q.Rampcode = x[i].Rampcode
				q.Region = x[i].Region

				q.TargetSize = m.TargetSize
				resultData = append(resultData, q)
			case "application":
				m = GetTargetsHealth(x[i].LoadName, x[i].Environment, x[i].Region)
				q.Provider = "AWS"
				q.ATSEngine = m.ATSEngine
				q.ATSItn = m.ATSItn
				q.ATSModel = m.ATSModel
				q.ATSServer = m.ATSServer
				q.Component = x[i].Component
				envval := x[i].Environment
				if envval == "prdeu" || envval == "prdkr" || envval == "prdsg" || envval == "prdus" {
					q.Environment = "prod"
				} else {
					q.Environment = envval
				}
				q.Healthy = m.Healthy
				q.Locale = x[i].Locale
				q.Rampcode = x[i].Rampcode
				q.Region = x[i].Region

				q.TargetSize = m.TargetSize
				resultData = append(resultData, q)
			case "ecs":
				m := getGeoEcsData(x[i].Environment, x[i].Region, x[i].LoadName)
				//				mainjson, err := json.MarshalIndent(m, "", "  ")
				//
				//				if err != nil {
				//					fmt.Println(err)
				//				}
				//				fmt.Printf("\n%v", string(mainjson))
				for _, element := range m {
					var p ServerMetadata
					p.Provider = element.Provider
					p.Component = "geo"
					p.Rampcode = "mobile"
					p.Environment = element.Environment
					p.Locale = element.Locale
					p.Region = element.Region
					p.TargetSize = element.TargetSize
					p.Healthy = element.Healthy
					p.GeoTaskArn = element.Arn
					localval := element.Locale
					switch localval {
					case "it":
						p.ItImage = element.ImageId
					case "de":
						p.DeImage = element.ImageId
					case "es":
						p.EsImage = element.ImageId
					case "fr":
						p.FrImage = element.ImageId
					}
					resultData = append(resultData, p)
				}

			case "default":
				fmt.Println("Does not belong to anyone")
			}
		}(i)
	}
	wg.Wait()
	jsonData, err := json.Marshal(resultData)

	c.Data(200, gin.MIMEJSON, jsonData)
}

type MetadataTag2 struct {
	ATSModel   string `json:"ATSModel, omitempty"`
	ATSEngine  string `json:"ATSEngine, omitempty"`
	Dedetool   string `json:"Dedetool, omitempty"`
	TargetSize int    `json:"TargetSize"`
	Healthy    int    `json:"Healthy"`
}

/*GetLoadbalancerHealthcheck() to be used in other functions References: */
func GetLoadbalancerHealthcheck(Elbname, Environment, region string) MetadataTag2 {
	ProfileMap := map[string]string{
		"eng":   "dev",
		"int":   "dev",
		"stgeu": "stg",
		"stg":   "stg",
		"sprf":  "stg",
		"prdkr": "prod",
		"prdus": "prod",
		"prdsg": "prod",
		"prdeu": "prod",
	}

	//fmt.Printf("Profile set %v", ProfileMap[Environment])
	sess, err := session.NewSessionWithOptions(
		session.Options{
			Profile: ProfileMap[Environment],
			Config: aws.Config{
				Region: aws.String(region),
			},
		},
	)
	if err != nil {
		fmt.Println(err)
	}
	svc := elb.New(sess)
	input := &elb.DescribeInstanceHealthInput{
		LoadBalancerName: aws.String(Elbname),
	}
	result, err := svc.DescribeInstanceHealth(input)
	if err != nil {
		fmt.Println(err)
	}
	TargetSize := len(result.InstanceStates)
	var Healthy []string
	if TargetSize > 0 {
		for _, item := range result.InstanceStates {
			if *item.State == "InService" {
				Healthy = append(Healthy, *item.InstanceId)
			}
		}
	}
	HealthyCount := len(Healthy)
	var p MetadataTag2
	if HealthyCount > 0 {
		y := GetInstanceMetadata(Environment, region, Healthy[0], ProfileMap[Environment])
		p.ATSModel = y.ATSModel
		p.ATSEngine = y.ATSEngine
		p.ATSItn = y.ATSItn
		p.ATSServer = y.ATSServer
		p.TTSEngine = y.TTSEngine
		p.TTSModel = y.TTSModel
		p.TTSServer = y.TTSServer
		p.Dedetool = y.Dedetool
		p.Itittool = y.Itittool
		p.Esestool = y.Esestool
		p.Engbtool = y.Engbtool
		p.Frfrtool = y.Frfrtool
		p.Ititmodel = y.Ititmodel
		p.Dedemodel = y.Dedemodel
		p.Esesmodel = y.Esesmodel
		p.Engbmodel = y.Engbmodel
		p.Frfrmodel = y.Frfrmodel

		p.TargetSize = TargetSize
		p.Healthy = HealthyCount
	} else {
		p.TargetSize = TargetSize
		p.Healthy = 0
	}

	return p
}

func GetTargetsHealth(Elbname, Environment, region string) MetadataTag2 {

	ProfileMap := map[string]string{
		"eng":   "dev",
		"int":   "dev",
		"stgeu": "stg",
		"stgus": "stg",
		"sprf":  "stg",
		"prdkr": "prod",
		"prdus": "prod",
		"prdsg": "prod",
		"prdeu": "prod",
	}
	//fmt.Printf("Profile set %v", ProfileMap[Environment])
	sess, err := session.NewSessionWithOptions(
		session.Options{
			Profile: ProfileMap[Environment],
			Config: aws.Config{
				Region: aws.String(region),
			},
		},
	)
	if err != nil {
		fmt.Println(err)
	}
	svc := elbv2.New(sess)
	input := &elbv2.DescribeTargetHealthInput{
		TargetGroupArn: aws.String(Elbname),
	}

	result, err := svc.DescribeTargetHealth(input)
	if err != nil {
		fmt.Println(err)
	}
	TargetSize := len(result.TargetHealthDescriptions)
	var Healthy []string
	if TargetSize > 0 {
		for _, item := range result.TargetHealthDescriptions {
			if *item.TargetHealth.State == "healthy" {
				Healthy = append(Healthy, *item.Target.Id)
			}
		}
	}
	HealthySize := len(Healthy)
	var p MetadataTag2
	if HealthySize > 0 {
		y := GetInstanceMetadata(Environment, region, Healthy[0], ProfileMap[Environment])
		p.ATSModel = y.ATSModel
		p.ATSEngine = y.ATSEngine
		p.ATSItn = y.ATSItn
		p.ATSServer = y.ATSServer
		p.TTSEngine = y.TTSEngine
		p.TTSModel = y.TTSModel
		p.TTSServer = y.TTSServer
		p.TargetSize = TargetSize

		p.Healthy = HealthySize

	} else {
		p.TargetSize = TargetSize
		p.Healthy = 0
	}
	return p
}

//Target Group Logic
type MetadataTag struct {
	ATSModel  string `json:"ATSModel, omitempty"`
	ATSEngine string `json:"ATSEngine, omitempty"`

	Dedetool string `json:"Dedetool, omitempty"`
}

func GetInstanceMetadata(Environment, region, InstanceId, Profilename string) MetadataTag {
	sess, err := session.NewSessionWithOptions(
		session.Options{
			Profile: Profilename,
			Config: aws.Config{
				Region: aws.String(region),
			},
		},
	)
	if err != nil {
		fmt.Println(err)
	}
	svc := ec2.New(sess)
	input := &ec2.DescribeTagsInput{
		Filters: []*ec2.Filter{
			{
				Name: aws.String("resource-id"),
				Values: []*string{
					aws.String(InstanceId),
				},
			},
		},
	}
	result, err := svc.DescribeTags(input)
	if err != nil {
		fmt.Println(err)
	}
	var ATSEngine, ATSmodel, dedetool string
	for _, item := range result.Tags {
		tagValue := *item.Key
		switch tagValue {
		case "ATS-engine":
			ATSEngine = *item.Value
		case "ATS-model":
			ATSmodel = *item.Value
		case "engine_infos":
			Ttsengine = *item.Value
		case "models_infos":
			TtsModel = *item.Value

		case "ATS_de_tool_version":
			dedetool = *item.Value

		}
	}

	returnValue := MetadataTag{
		ATSEngine: ATSEngine,
		ATSModel:  ATSmodel,
		Dedetool:  dedetool,
	}
	return returnValue
}

type GeoMetadata struct {
	Provider    string `json:"Provider"`
	Arn         string `json:"Arn"`
	Environment string `json:"Environment"`
	Region      string `json:"Region"`
	Locale      string `json:"Locale"`
	ImageId     string `json:"Image"`
	TargetSize  int    `json:"TargetSize"`
	Healthy     int    `json:"Healthy"`
	Status      string `json:"Status"`
}

func getGeoEcsData(Environment, Region, clustername string) []GeoMetadata {
	ProfileMap := map[string]string{
		"eng":   "dev",
		"int":   "dev",
		"stgeu": "stg",
		"stgus": "stg",
		"sprf":  "stg",
		"prdkr": "prod",
		"prdus": "prod",
		"prdsg": "prod",
		"prdeu": "prod",
	}
	//fmt.Printf("Profile set %v", ProfileMap[Environment])
	sess, err := session.NewSessionWithOptions(
		session.Options{
			Profile: ProfileMap[Environment],
			Config: aws.Config{
				Region: aws.String(Region),
			},
		},
	)
	if err != nil {
		fmt.Println(err)
	}
	svc := ecs.New(sess)
	input := &ecs.ListTasksInput{
		Cluster: aws.String(clustername),
	}
	result, err := svc.ListTasks(input)
	if err != nil {
		fmt.Println(err)
		fmt.Println("Error occurred in list tasks")
	}

	var x []GeoMetadata

	for _, item := range result.TaskArns {
		var y GeoMetadata
		taskuuid := strings.Split(*item, "/")[1]
		//fmt.Println(taskuuid)
		y.Provider = "AWS"
		y.Region = Region
		y.Environment = Environment
		y.Arn = *item
		y.TargetSize = 2
		y.Healthy = 2

		input2 := &ecs.DescribeTasksInput{
			Tasks: []*string{
				aws.String(taskuuid),
			},
			Cluster: aws.String(clustername),
		}
		result, err := svc.DescribeTasks(input2)
		if err != nil {
			fmt.Println(err)
			fmt.Println("Error occurred in describe tasks")
		}
		for _, item2 := range result.Tasks {
			var imageid string
			for _, element := range item2.Containers {
				imageid = *element.Image
			}
			y.ImageId = imageid
			ServiceGrouptemp1 := item2.Group
			ServiceGrouptemp2 := strings.Split(*ServiceGrouptemp1, ":")[1]
			ServiceGroup := strings.Split(ServiceGrouptemp2, "-")[2]
			//fmt.Println(ServiceGroup)
			y.Locale = ServiceGroup
			y.Status = *item2.LastStatus

		}
		//fmt.Printf("\n%v", y)
		x = append(x, y)
	}
	return x

}
