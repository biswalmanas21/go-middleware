package aws

type AWSConfig struct {
	Component    string `json:"Component"`
	Rampcode     string `json:"Rampcode"`
	Locale       string `json:"Locale"`
	Environment  string `json:"Environment"`
	ResourceType string `json:"ResourceType"`
	LoadName     string `json:"LoadName"`
	Region       string `json:"Region"`
}

type ServerMetadata struct {
	Provider     string `json:"Provider, omitempty"`
	Component    string `json:"Component"`
	Environment  string `json:"Environment"`
	Locale       string `json:"Locale, omitempty"`
	Rampcode     string `json:"Rampcode, omitempty"`
	Region       string `json:"Region"`
	TargetSize   int    `json:"TargetSize"`
	Healthy      int    `json:"Healthy"`
	ASRModel     string `json:"ASRModel, omitempty"`
	ASREngine    string `json:"ASREngine, omitempty"`
	ASRItn       string `json:"ASRItn, omitempty"`
	ASRServer    string `json:"ASRServer, omitempty"`
	TTSModel     string `json:"TTSModel, omitempty"`
	TTSEngine    string `json:"TTSEngine, omitempty"`
	TTSServer    string `json:"TTSServer, omitempty"`
	DeDETool     string `json:"DeDETool, omitempty"`
	FrFRTool     string `json:"FrFRTool, omitempty"`
	EsESTool     string `json:"EsESTool, omitempty"`
	EnGBTool     string `json:"EnGBTool, omitempty"`
	ItITTool     string `json:"ItITTool, omitempty"`
	DeDEModel    string `json:"DeDEModel, omitempty"`
	FrFRModel    string `json:"FrFRModel, omitempty"`
	EsESModel    string `json:"EsESModel, omitempty"`
	EnGBModel    string `json:"EnGBModel, omitempty"`
	ItITModel    string `json:"ItITModel, omitempty"`
	DeDEGeoImage string `json:"DeDEGeoImage, omitempty"`
	ItITGeoImage string `json:"ItITGeoImage, omitempty"`
	FrFRGeoImage string `json:"FrFRGeoImage, omitempty"`
	EsESGeoImage string `json:"EsESGeoImage, omitempty"`
	GeoTaskArn   string `json:"TaskArn,omitempty"`

	Messages string `json:"Message, omitempty"`
}
