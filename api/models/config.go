package models

import (
	"marinaserver/config"
	"os"
)

var server = os.Getenv("DATABASE")
var databaseName = os.Getenv("DATABASE_NAME")

// var server = "localhost"
// var databaseName = "marinaconfig"

var dbConnect = config.NewConnection(server, databaseName)
