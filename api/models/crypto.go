package models

import (
	"marinaserver/api/forms"

	"gopkg.in/mgo.v2/bson"
)

type Crypto struct {
	ID        bson.ObjectId `json:"_Id" bson:"_Id"`
	HashValue string        `json:"Password" bson:"Password"`
}

type CryptoModel struct{}

func (e *CryptoModel) GetCryptoVal(data forms.Crypto) (expectedMetadata Crypto, err error) {

	collection := dbConnect.Use(databaseName, "bcrypts")
	err = collection.Find(bson.M{"HashValue": data.HashValue}).One(&expectedMetadata)
	return expectedMetadata, err

}
