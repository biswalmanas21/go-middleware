package models

import (
	"marinaserver/api/forms"

	"gopkg.in/mgo.v2/bson"
	//"fmt"
)

// ExpectedCount to be used in functions outside models package
// type ExpectedCount struct {
// 	ID           bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty"`
// 	ExpectedList []ExpectedVal `json:"Expected" bson:"Expected"`
// }

// ExpectedVal to be used in functions outside models package
type ExpectedVal struct {
	ID          bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty"`
	Provider    string        `json:"Provider" bson:"Provider"`
	Component   string        `json:"Component" bson:"Component"`
	Rampcode    string        `json:"Rampcode" bson:"Rampcode"`
	Locale      string        `json:"Locale" bson:"Locale"`
	Environment string        `json:"Environment" bson:"Environment"`
	Region      string        `json:"Region" bson:"Region"`
	Expected    int           `json:"Expected" bson:"Expected"`
	BfrTime     int           `json:"BfrTime" bson:"BfrTime"`
}

type ExpectedProviderModel struct{}

func (e *ExpectedProviderModel) UpdateExpected(data forms.UpdateExpectedValue) error {

	collection := dbConnect.Use(databaseName, "expected")
	err := collection.Insert(bson.M{
		"Provider":    data.Provider,
		"Component":   data.Component,
		"Rampcode":    data.Rampcode,
		"Locale":      data.Locale,
		"Environment": data.Environment,
		"Region":      data.Region,
		"Expected":    data.Expected,
		"BfrTime":     data.BfrTime,
	})
	return err
}

func (e *ExpectedProviderModel) GetExpectedValues() (expectedMetadata []ExpectedVal, err error) {

	collection := dbConnect.Use(databaseName, "expected")
	err = collection.Find(nil).All(&expectedMetadata)
	return expectedMetadata, err

}

func (e *ExpectedProviderModel) GetExpectedResult(data forms.UpdateExpectedValue) (expectedMetadata []ExpectedVal, err error) {
	collection := dbConnect.Use(databaseName, "expected")
	err = collection.Find(bson.M{
		"Provider":    data.Provider,
		"Component":   data.Component,
		"Rampcode":    data.Rampcode,
		"Locale":      data.Locale,
		"Environment": data.Environment,
		"Region":      data.Region,
		"Expected":    data.Expected,
		"BfrTime":     data.BfrTime,
	}).All(&expectedMetadata)
	return expectedMetadata, err
}

func (e *ExpectedProviderModel) UpdateExisting(data forms.UpdateExisting) (expectedMetadata ExpectedVal, err error) {
	collection := dbConnect.Use(databaseName, "expected")
	//fmt.Printf("%v", data.ID)
	_, err = collection.Upsert(bson.M{"_id": bson.ObjectIdHex(data.ID)}, bson.M{"$set": bson.M{
		"Provider":    data.Provider,
		"Component":   data.Component,
		"Rampcode":    data.Rampcode,
		"Locale":      data.Locale,
		"Environment": data.Environment,
		"Region":      data.Region,
		"Expected":    data.Expected,
		"BfrTime":     data.BfrTime,
	}})

	return expectedMetadata, err
}
