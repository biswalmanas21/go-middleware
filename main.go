package main

import (
	// Log items to the terminal
	"log"

	"github.com/gin-gonic/gin"

	"marinaserver/api/controllers/aws"
	"marinaserver/api/controllers/expected"
	"marinaserver/api/controllers/gcp"

	"github.com/gin-contrib/cors"
	"github.com/joho/godotenv"
)

func init() {
	handlers := gin.New()
	handlers.Use(gin.Recovery())
	if err := godotenv.Load(); err != nil {
		log.Printf("No .env file found")
	}
}

func main() {
	// Init gin router
	router := gin.Default()
	router.Use(cors.Default())

	// Its great to version your API's
	v1 := router.Group("/api/v1")
	{
		Expected := new(expected.ExpectedController)
		crypto := new(expected.CryptoController)
		gcpc := new(gcp.GcpController)
		awsc := new(aws.AwsController)

		v1.POST("/cxpected", Expected.UpdateExpected)
		v1.GET("/expected", Expected.GetExpected)
		v1.GET("/oneexpected", Expected.FindOne)
		v1.POST("/updateexisting", Expected.FindnUpdate)
		v1.GET("/crypto/:password", crypto.FindCryptoOne)
		v1.GET("/gcpversions", gcpc.GetData)
		v1.GET("/awsversions", awsc.GetData)
	}

	router.NoRoute(func(c *gin.Context) {
		c.JSON(404, gin.H{"message": "Not found"})
	})

	// Init our server
	router.Run(":5000")
}
