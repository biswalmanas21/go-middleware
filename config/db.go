package config

import (
	"os"
	"time"

	"gopkg.in/mgo.v2"
)

type DBConnection struct {
	session *mgo.Session
}

func NewConnection(host string, dbName string) (conn *DBConnection) {
	info := &mgo.DialInfo{

		Addrs: []string{host},

		Timeout: 60 * time.Second,

		Database: dbName,

		Username: os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PWD"),
		// Username: os.Getenv(""),
		// Password: os.Getenv(""),
	}

	session, err := mgo.DialWithInfo(info)

	if err != nil {
		panic(err)
	}

	session.SetMode(mgo.Monotonic, true)
	conn = &DBConnection{session}
	return conn
}

// Use handles connect to a certain collection
func (conn *DBConnection) Use(dbName, tableName string) (collection *mgo.Collection) {
	// This returns method that interacts with a specific collection and table
	return conn.session.DB(dbName).C(tableName)
}

// Close handles closing a database connection
func (conn *DBConnection) Close() {
	// This closes the connection
	conn.session.Close()
	return
}
